<?php

namespace App\DataTables;

use App\Admin;
use Yajra\DataTables\Services\DataTable;

class AdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', 'admin.admins.btn.edit')
            ->addColumn('delete', 'admin.admins.btn.delete')
            ->rawColumns([
                'edit',
                'delete'
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Admin::query();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
//                    ->addAction(['width' => '80px'])
//                    ->parameters($this->getBuilderParameters());
                    ->parameters([
                        'dom'           => 'Bfrtip',
                        'lengthMenu'    => [[10, 25, 50, 100, -1], [10, 25, 50, 'All Record']],
                        'buttons'       => [
                            ['text'          => '<i class="fa fa-plus"></i>','className'     => 'btn btn-info','title'         => 'Create Admin'],
                            ['extend' => 'csv', 'className' => 'btn btn-info margin', 'text' => '<li class="fa fa-file"></li>'],
                            ['extend' => 'excel', 'className' => 'btn bg-olive btn-flat margin', 'text' => '<li class="fa fa-file-excel-o"></li>'],
                            ['extend' => 'print', 'className' => 'btn btn-primary margin', 'text' => '<li class="fa fa-print"></li>'],
                            ['extend' => 'reload', 'className' => 'btn bg-purple margin', 'text' => '<li class="fa fa-refresh"></li>'],
                        ]

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name'  => 'id',
                'data'  => 'id',
                'title' => 'ID',
            ],[
                'name'  => 'name',
                'data'  => 'name',
                'title' => 'Admin Name',
            ],[
                'name'  => 'email',
                'data'  => 'email',
                'title' => 'Admin Email',
            ],[
                'name'  => 'created_at',
                'data'  => 'created_at',
                'title' => 'Created at',
            ],[
                'name'  => 'updated_at',
                'data'  => 'updated_at',
                'title' => 'Updated at',
            ],[
                'name'          => 'edit',
                'data'          => 'edit',
                'title'         => 'Edit',
                'exportable'    => false,
                'printable'     => false,
                'orderable'     => false,
                'searchable'    => false,
            ],[
                'name'          => 'delete',
                'data'          => 'delete',
                'title'         => 'Delete',
                'exportable'    => false,
                'printable'     => false,
                'orderable'     => false,
                'searchable'    => false,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin_' . date('YmdHis');
    }
}
