<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\AdminResetPassword;
use Carbon\Carbon;
use App\Admin;
use DB;
use Mail;

class AdminAuth extends Controller
{
    public function login(){
        return view('admin.login');
    }

    public function doLogin(){
        $rememberMe = request('rememberMe') == 1 ? true : false ;
        if(auth()->guard('admin')->attempt(['email' => request('email'), 'password' => request('password')], $rememberMe)){
            return redirect(aurl());
        }else{
            session()->flash('error', trans('admin.inccorrect_information_login'));
            return redirect(aurl('login'));
        }
    }
    public function logout(){
        auth()->guard('admin')->logout();
        return redirect(aurl('login'));
    }

    public function showForgotPassword(){
        return view('admin.forgotPassword');
    }
    public function handleForgotPassword(){
        $admin = Admin::where('email', request('email'))->first();
        if(!empty($admin)){
            $token = app('auth.password.broker')->createToken($admin);
            $data = DB::table('password_resets')->insert([
                'email' => $admin->email,
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);
            Mail::to('Marouene.Tayari@biat.com.tn')->send(new AdminResetPassword(['data' => $admin, 'token' => $token]));
            session()->flash('success', trans('admin.the_link_reset_sent'));

        }
        return back();
    }

    public function showResetPassword($token){
        $checkToken = DB::table('password_resets')->where('token', $token)
        ->where('created_at', '>', Carbon::now()->subHours(2))->first();

        if(!empty($checkToken)){
            return view('admin.resetPassword', ['data' => $checkToken]);
        }else{
            return redirect(aurl('forgot/password'));
        }
    }

    public function handleResetPassword($token){
        //Validate informations
        $this->validate(request(), [
            'password'              => 'required|min:6|same:passwordConfirmation',
            'passwordConfirmation'  => 'required'
        ], [], [
            'password'              => 'Password',
            'passwordConfirmation'  => 'Confirmation Password'
        ]);


        $checkToken = DB::table('password_resets')->where('token', $token)
            ->where('created_at', '>', Carbon::now()->subHours(2))->first();

        if(!empty($checkToken)){
            $admin = Admin::where('email', $checkToken->email)->update(['email' => $checkToken->email,
            'password' => bcrypt(request('password'))]);

            DB::table('password_resets')->where('email', request('email'))->delete();
            admin()->attempt(['email' => $checkToken->email, 'password' => request('password')], true);
            return redirect(aurl());

        }else{
            return redirect(aurl('forgot/password'));
        }

    }
}
