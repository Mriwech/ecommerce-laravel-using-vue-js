<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
    Config::set('auth.defines', 'admin');

    Route::get('login', 'AdminAuth@login');
    Route::post('login', 'AdminAuth@doLogin');

    Route::get('forgot/password', 'AdminAuth@showForgotPassword');
    Route::post('forgot/password', 'AdminAuth@handleForgotPassword');

    Route::get('reset/password/{token}', 'AdminAuth@showResetPassword');
    Route::post('reset/password/{token}', 'AdminAuth@handleResetPassword');

    Route::group(['middleware' => 'admin:admin'], function(){
        Route::resource('admin', 'AdminController');
        Route::get('/', function () {
            //TODO : Get Machin name / Emp. Matricule / User IP
            /*
            dump("Nom : ".shell_exec("echo %username%"));
            dump("Machine : ".shell_exec("echo %COMPUTERNAME%"));
            dump("Adresse IP : ". getHostByName(php_uname('n')));
            dump(getHostByName(getHostName()));
            dump(gethostbyname(trim(exec("hostname"))));
            */
            return view('admin.home');
        });

        Route::any('logout', 'AdminAuth@logout');

    });



});

