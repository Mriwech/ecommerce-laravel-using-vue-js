<?php

return [
    'login' => 'Login',
    'admin' => 'Admin account', //Left side barre
    'adminPanel' => 'Admin Panel',
    'inccorrect_information_login' => 'Email or password incorrect Please try again !',
    'forgot_password' => 'Forgot password',
    'the_link_reset_sent' => 'Reset Link is Sent',
];
